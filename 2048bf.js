function fireKey(el, key) {
    if(document.createEventObject) {
        var eventObj = document.createEventObject();
        eventObj.keyCode = key;
        el.fireEvent("onkeydown", eventObj);   
    }else if(document.createEvent) {
        var eventObj = document.createEvent("Events");
        eventObj.initEvent("keydown", true, true);
        eventObj.which = key;
        el.dispatchEvent(eventObj);
    }
}

var keys = [40, 37];
    right = [39, 37, 40],
    up = [37, 38],
    score = document.getElementsByClassName('score-container')[0].innerHTML.replace(/(<([^>]+)>)/ig,""),
    boolean = true,
    movedRight = 0,
    lastTimeScoreDidntChange = 0;

window.$ = $;

var run = function (runkeys) {
console.log(runkeys);
    for (i = 0; i < runkeys.length; i++) {
        fireKey($('.game-container'), runkeys[i]);

        if (document.getElementsByClassName('game-over').length) {
            document.getElementsByClassName('retry-button')[0].click();
        }

        if (i == runkeys.length - 1) {
            // Last in array of key events
            setTimeout(function () {

                if (score == document.getElementsByClassName('score-container')[0].innerHTML.replace(/(<([^>]+)>)/ig,"")) {
                    // If score didn't change
                    lastTimeScoreDidntChange++;
                } else {
                    // If score changed
                    lastTimeScoreDidntChange = 0;
                }

                if (lastTimeScoreDidntChange <= 10) {
                    run(keys)
                } else {
                    if (movedRight <= 8) {
                        ++movedRight;
                        run(right);
                    } else if (movedRight == 9) {
                        movedRight = 0;
                        run(up);
                    } else {
                        movedRight = 0;
                    }
                    lastTimeScoreDidntChange = 0;
                }
            }, 80);
        }

        score = document.getElementsByClassName('score-container')[0].innerHTML.replace(/(<([^>]+)>)/ig,"");
    }
}

run(keys);